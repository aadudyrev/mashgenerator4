//
//  ADSquare.h
//  ADComandTest
//
//  Created by Александр Дудырев on 13.10.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#ifndef ADSquare_h
#define ADSquare_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct ADPoint {
    int x;
    int y;
};

struct ADSquare {
    struct ADPoint point;
    bool isBlock;
    bool selected;
};

bool squareIsAvailible(struct ADSquare *);

void printSquare(struct ADSquare *);

#endif /* ADSquare_h */
