//
//  ADSquare.c
//  ADComandTest
//
//  Created by Александр Дудырев on 13.10.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#include "ADSquare.h"

bool squareIsAvailible(struct ADSquare *s) {
    return !(s->isBlock || s->selected);
}

void printSquare(struct ADSquare *s) {
    printf("x: %d, y: %d, selected: %d, isBlock: %d\n", s->point.x, s->point.y, s->selected, s->isBlock);
}
