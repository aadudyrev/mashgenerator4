//
//  ADField.c
//  ADComandTest
//
//  Created by Александр Дудырев on 13.10.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#include "ADField.h"
#include "ADSquare.h"

struct ADSquare *squareAtPoint(struct ADField *field, struct ADPoint point);

struct ADField *createField(int sideSize) {
    int squaresCount = sideSize * sideSize;
    
    struct ADSquare *squares = malloc(sizeof(struct ADSquare) * squaresCount);
    for (int i = 0; i < squaresCount; i++) {
        struct ADSquare square;
        square.isBlock = false;
        square.selected = false;
        square.point.x = i % sideSize;
        square.point.y = i / sideSize;
        
        squares[i] = square;
    }
    
    struct ADField *field = malloc(sizeof(struct ADField));
    field->sideSize = sideSize;
    field->squares = squares;
    
    return field;
}

void freeField(struct ADField *field) {
    free(field->squares);
    free(field);
}

void clearSelection(struct ADField *field) {
    int squaresCount = field->sideSize * field->sideSize;
    for (int i = 0; i < squaresCount; i++) {
        struct ADSquare *s = squareAtIndex(field, i);
        s->selected = false;
    }
}

bool fieldAvailibleToEndPath(struct ADField *field, struct ADSquare *lastS, ADFieldDirection_t direction) {
    struct ADSquare *tmpS = NULL;
    struct ADPoint tmpPoint;
    struct ADPoint const point = lastS->point;
    int sizeSide = field->sideSize;
    
    if (direction == ADFieldDirectionTop || direction == ADFieldDirectionBottom) {
        tmpPoint.x = point.x;
        for (int y = 0; y < sizeSide; y++) {
            tmpPoint.y = y;
            tmpS = squareAtPoint(field, tmpPoint);
            if (squareIsAvailible(tmpS)) {
                return true;
            }
        }
        
        for (int i = 0; i < sizeSide * sizeSide; i++) {
            tmpS = squareAtIndex(field, i);
            if (tmpS->point.x == tmpPoint.x) {
                continue;
            }
            if (squareIsAvailible(tmpS)) {
                return true;
            }
        }
        
    } else {
        tmpPoint.y = point.y;
        for (int x = 0; x < sizeSide; x++) {
            tmpPoint.x = x;
            tmpS = squareAtPoint(field, tmpPoint);
            if (squareIsAvailible(tmpS)) {
                return true;
            }
        }
        
        for (int i = 0; i < sizeSide * sizeSide; i++) {
            tmpS = squareAtIndex(field, i);
            if (tmpS->point.y == tmpPoint.y) {
                continue;
            }
            if (squareIsAvailible(tmpS)) {
                return true;
            }
        }
    }
    
    return false;
}

bool allSelected(struct ADField *field) {
    int squaresCount = field->sideSize * field->sideSize;
    for (int i = 0; i < squaresCount; i++) {
        struct ADSquare square = field->squares[i];
        if (!square.selected && !square.isBlock) {
            return false;
        }
    }
    return true;
}

int indexOfSquare(struct ADField *field, struct ADSquare *s) {
    struct ADPoint point = s->point;
    int index = point.y * field->sideSize + point.x;
    return index;
}

struct ADSquare *squareAtIndex(struct ADField *field, int index) {
    return &field->squares[index];
}

struct ADSquare *squareAtPoint(struct ADField *field, struct ADPoint point) {
    int index = point.y * field->sideSize + point.x;
    return squareAtIndex(field, index);
}

struct ADSquare *nextSquareAtDirection(ADFieldDirection_t direction, struct ADField *field, struct ADSquare *s) {
    struct ADSquare *nextS = NULL;
    struct ADPoint nextPoint = s->point;
    
    switch (direction) {
        case ADFieldDirectionTop:
            if (nextPoint.y > 0) {
                nextPoint.y--;
                nextS = squareAtPoint(field, nextPoint);
            }
            break;
        case ADFieldDirectionLeft:
            if (nextPoint.x > 0) {
                nextPoint.x--;
                nextS = squareAtPoint(field, nextPoint);
            }
            break;
        case ADFieldDirectionBottom:
            if (nextPoint.y < (field->sideSize) - 1) {
                nextPoint.y++;
                nextS = squareAtPoint(field, nextPoint);
            }
            break;
        case ADFieldDirectionRight:
            if (nextPoint.x < (field->sideSize) - 1) {
                nextPoint.x++;
                nextS = squareAtPoint(field, nextPoint);
            }
            break;
        default:
            break;
    }
    
    return nextS;
}

void printField(struct ADField *field) {
    for (int i = 0; i < field->sideSize * field->sideSize; i++) {
        struct ADSquare *square = squareAtIndex(field, i);
        printSquare(square);
    }
}


