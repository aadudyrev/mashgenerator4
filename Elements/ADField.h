//
//  ADField.h
//  ADComandTest
//
//  Created by Александр Дудырев on 13.10.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#ifndef ADField_h
#define ADField_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum ADFieldDirection {
    ADFieldDirectionTop,
    ADFieldDirectionLeft,
    ADFieldDirectionBottom,
    ADFieldDirectionRight,
};

typedef enum ADFieldDirection ADFieldDirection_t;

struct ADField {
    
    // размер стороны квадрата
    int sideSize;
    
    // квадраты, из которых состоит поле
    struct ADSquare *squares;
};

// новое поле с размером
struct ADField *createField(int);

// отчистить память
void freeField(struct ADField *);

// проверка на возможность завершить поиск пути (зависит от последнео направления движения)
bool fieldAvailibleToEndPath(struct ADField *, struct ADSquare *, ADFieldDirection_t);

// возвращает true, если все квадраты выбраны
bool allSelected(struct ADField *);

// отчистить выделение элементов
void clearSelection(struct ADField *);

// индекс по объекту
int indexOfSquare(struct ADField *field, struct ADSquare *s);

// квадрат по индексу
struct ADSquare *squareAtIndex(struct ADField *, int);

// вернет первый соседний (по направлению) элемент от данного
struct ADSquare *nextSquareAtDirection(ADFieldDirection_t, struct ADField *, struct ADSquare *);

void printField(struct ADField *);


#endif /* ADField_h */
