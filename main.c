//
//  main.m
//  ADComandTest
//
//  Created by Александр Дудырев on 08.07.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#include <stdlib.h>
#include <pthread.h>

#include "ADSearchEngine.h"

struct DataWrapper {
    int size;
    DivideType type;
};

pthread_t runThread(int count, DivideType type);
void *calcVarts(void *args);

int main() {
    
    pthread_t thrd9_5 = runThread(9, DivideFive);
    pthread_t thrd9_3 = runThread(9, DivideThree);
    pthread_t thrd9_2 = runThread(9, DivideTwo);
    pthread_t thrd9_1 = runThread(9, DivideOne);
    pthread_join(thrd9_5, NULL);
    pthread_join(thrd9_3, NULL);
    pthread_join(thrd9_2, NULL);
    pthread_join(thrd9_1, NULL);
    
    return 0;
}

pthread_t runThread(int count, DivideType type) {
    
    struct DataWrapper *dataWrapper = malloc(sizeof(dataWrapper));
    dataWrapper->size = count;
    dataWrapper->type = type;
    
    pthread_t thrd;
    int status = pthread_create(&thrd, NULL, calcVarts, dataWrapper);
    printf("thrd status: %d, count: %d\n", status, count);

    return thrd;
}

void *calcVarts(void *args) {
    struct DataWrapper *dataWrapper = args;
    
    printf("prepareVariants: %d, type: %d\n", dataWrapper->size, dataWrapper->type);

    prepareVariants(dataWrapper->size, dataWrapper->type);
    
    pthread_exit(0);
}
