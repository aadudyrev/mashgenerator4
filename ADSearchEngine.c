#include "ADSearchEngine.h"
#include "Elements/ADField.h"
#include "Elements/ADSquare.h"
#include "FileManager/ADFileManager.h"

struct ADCombinationList {
    int sideSize;
    int **indexes;
    int count;
};

void createVariants(int *blockIndexes, int blockCount, int blockNeeded, struct ADCombinationList *, bool, DivideType);
void createConcreteVariant(int *blockIndexes, int blockCount, struct ADCombinationList *);
DivideType foundDivideType(int number);

struct ADSquare *searchPath(struct ADField *field, int sideSize, int squareCount);
bool searchPathAtSquare(struct ADField *field, struct ADSquare *s);
bool searchPathAtSquareWithDirection(struct ADField *field, struct ADSquare *s, ADFieldDirection_t directon);

void printBlockIndexes(int *blockIndexes, int blockCount, int sideSize);


void prepareVariants(int sideSize, DivideType type) {
    for (int i = MIN_BLOCK_NUMBER; i < MAX_BLOCK_NUMBER; i++) {
        // для конкретного колличества блоков просчитать все варианты полей
        int *blockIndexes;
        
        struct ADCombinationList list;
        list.sideSize = sideSize;
        list.indexes = malloc(sizeof(list.indexes) * BUFFER_COUNT);
        list.count = 0;
        
        for (int i = 0; i < BUFFER_COUNT; i++) {
            *(list.indexes + i) = malloc(sizeof(int) * (i + 1));
        }
        
        int *lastRecord = lastBlockIndexes(sideSize, i, type);
        if (lastRecord != NULL) {
            blockIndexes = lastRecord;
            createVariants(blockIndexes, i, i, &list, true, type);
        } else {
            blockIndexes = malloc(sizeof(int) * i);
            createVariants(blockIndexes, i, i, &list, false, type);
        }

        writeCombination(sideSize, list.indexes, list.count, i + 1, type);
        
        for (int i = 0; i < BUFFER_COUNT; i++) {
            free(*(list.indexes + i));
        }
        free(list.indexes);
        free(blockIndexes);
    }
}

void createVariants(int *blockIndexes, int blockCount, int blockNeeded, struct ADCombinationList *list, bool resumeSearch, DivideType type) {
    
    int sideSize = list->sideSize;
    int beginPosition = 0;
    int index = blockCount - blockNeeded;
    
    if (resumeSearch == true) {
        
        beginPosition = blockIndexes[index];
        
    } else {
        
        if (index > 0) {
            beginPosition = blockIndexes[index - 1] + 1;
        }
    }
    
    for (int i = beginPosition; i <= sideSize * sideSize - blockNeeded; i++) {
        
        switch (type) {
            case DivideFive:
                if (i % 5 != 0) {
                    continue;
                }
                break;
            case DivideThree:
                if (i == 0 || i % 3 != 0) {
                    continue;
                }
                break;
            case DivideTwo:
                if (i == 0 || i % 2 != 0) {
                    continue;
                }
                break;
            case DivideOne:
                if (i == 0 || i % 2 == 0) {
                    continue;
                }
                break;
            case DivideDefault:
                break;
        }
        
        blockIndexes[index] = i;
        
        if ((blockNeeded - 1) > 0) {
            
            createVariants(blockIndexes, blockCount, blockNeeded - 1, list, resumeSearch, DivideDefault);
            resumeSearch = false;
            
        } else {
            
            createConcreteVariant(blockIndexes, blockCount, list);
        }
    }
}

void createConcreteVariant(int *blockIndexes, int blockCount, struct ADCombinationList *list) {
    int sideSize = list->sideSize;
    struct ADField *field = createField(sideSize);
    
    for (int i = 0; i < blockCount; i++) {
        int blockIndex = blockIndexes[i];
        struct ADSquare *s = squareAtIndex(field, blockIndex);
        s->isBlock = true;
    }
    
    int squareCount = sideSize * sideSize - blockCount;
    struct ADSquare *s = searchPath(field, sideSize, squareCount);
    
    if (s != NULL) {
        
        int *indexes = *(list->indexes + list->count);
        int count = 0;
        while (count < blockCount + 1) {
            int index;
            if (count < blockCount) {
                index = *(blockIndexes + count);
            } else {
                index = indexOfSquare(field, s);
            }
            *(indexes + count) = index;
            
            count++;
        }
        list->count++;

        if (list->count == BUFFER_COUNT) {
            writeCombination(sideSize, list->indexes, BUFFER_COUNT, blockCount + 1, foundDivideType(blockIndexes[0]));
            printBlockIndexes(*(list->indexes + list->count - 1), blockCount + 1, sideSize);
            list->count = 0;
        }
    }
    
    freeField(field);
}

DivideType foundDivideType(int number) {
    
    if (number % 5 == 0) {
        return DivideFive;
    }
    if (number % 3 == 0) {
        return DivideThree;
    }
    if (number % 2 == 0) {
        return DivideTwo;
    }
    if (number % 2 == 1) {
        return DivideOne;
    }
    
    return DivideDefault;
}

void printBlockIndexes(int *blockIndexes, int blockCount, int sideSize) {
    printf("block Indexes: ");
    for (int i = 0; i < blockCount; i++) {
        printf("%d, ", blockIndexes[i]);
    }
    printf("\tfor sideSize: %d\t blockCount: %d\n", sideSize, blockCount - 1);
}

struct ADSquare *searchPath(struct ADField *field, int sideSize, int squareCount) {
    
    for (int i = 0; i < squareCount; i++) {
        struct ADSquare *s = squareAtIndex(field, i);
        
        clearSelection(field);
        
        if (!squareIsAvailible(s)) {
            continue;
        }
        
        // call search alg
        bool pathExist = searchPathAtSquare(field, s);
        if (pathExist) {
            return s;
        }
    }
    
    return NULL;
}

bool searchPathAtSquare(struct ADField *field, struct ADSquare *s) {
    bool tmpPathIndexes = false;
    
    tmpPathIndexes = searchPathAtSquareWithDirection(field, s, ADFieldDirectionTop);
    if (tmpPathIndexes) {
        return tmpPathIndexes;
    }
    
    tmpPathIndexes = searchPathAtSquareWithDirection(field, s, ADFieldDirectionLeft);
    if (tmpPathIndexes) {
        return tmpPathIndexes;
    }
    
    tmpPathIndexes = searchPathAtSquareWithDirection(field, s, ADFieldDirectionBottom);
    if (tmpPathIndexes) {
        return tmpPathIndexes;
    }
    
    tmpPathIndexes = searchPathAtSquareWithDirection(field, s, ADFieldDirectionRight);
    if (tmpPathIndexes) {
        return tmpPathIndexes;
    }

    return false;
}

bool searchPathAtSquareWithDirection(struct ADField *field, struct ADSquare *s, ADFieldDirection_t directon) {
    // массив длинной в сторону квадрата для выделенных элементов
    int count = field->sideSize;
    struct ADSquare **currentSelectedSquares = malloc(sizeof(currentSelectedSquares) * count);
    
    int currentSelectedSquareCount = 0;
    
    s->selected = true;
    *(currentSelectedSquares + currentSelectedSquareCount++) = s;
    
    struct ADSquare *nextS = nextSquareAtDirection(directon, field, s);
    while (nextS != NULL && squareIsAvailible(nextS)) {
        *(currentSelectedSquares + currentSelectedSquareCount++) = nextS;
        
        nextS->selected = true;
        nextS = nextSquareAtDirection(directon, field, nextS);
    }
    
    if (currentSelectedSquareCount > 1) {
        // не будем выделять последний квадрат
        nextS = *(currentSelectedSquares + --currentSelectedSquareCount);
        nextS->selected = false;
        
        if (!searchPathAtSquare(field, nextS)) {
            struct ADSquare *tmpS = NULL;
            for (int i = 0; i <= currentSelectedSquareCount; i++) {
                tmpS = *(currentSelectedSquares + i);
                tmpS->selected = false;
            }
            
            free(currentSelectedSquares);
            return false;
        }
    }
    
    free(currentSelectedSquares);
    
    if (allSelected(field)) {
        return true;
    }
    
    s->selected = false;
    
    return false;
}











