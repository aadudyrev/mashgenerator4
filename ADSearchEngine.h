//
//  ADSearchEngine.h
//  ADComandTest
//
//  Created by Александр Дудырев on 13.10.2018.
//  Copyright © 2018 Александр Дудырев. All rights reserved.
//

#ifndef ADSearchEngine_h
#define ADSearchEngine_h

#include <stdio.h>

#include "FileManager/ADConstants.h"

void prepareVariants(int sideSize, DivideType);

#endif /* ADSearchEngine_h */
