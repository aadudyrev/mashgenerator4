#ifndef ADFileManager_h
#define ADFileManager_h

#include "ADConstants.h"

// последняя записанная комбинаация или NULL
int *lastBlockIndexes(int sideSize, int blockCount, DivideType type);

// записать комбинацию в файл
void writeCombination(int sideSize, int **indexes, int countOfIndexes, int countInRecord, DivideType type);

#endif
