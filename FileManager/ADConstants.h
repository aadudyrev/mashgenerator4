#ifndef ADConstants_h
#define ADConstants_h

#include <stdio.h>

#define BUFFER_COUNT 1
#define MIN_BLOCK_NUMBER 5
#define MAX_BLOCK_NUMBER 6

#define BASE_PATH "/Users/admin/Documents/Results/"
#define SEPARATOR '|'
#define END_OF_RECORD '!'

typedef enum {
    DivideFive = 5,
    DivideThree = 3,
    DivideTwo = 2,
    DivideOne = 1,
    DivideDefault = 0,
} DivideType;

#endif /* ADConstants_h */
