#include "ADFileManager.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char *currentPath(int, int, DivideType);
bool checkLastStr(char *);
bool checkTerminator(char c);
void copyFile(char *sourceName, char *destName, int strCount);


int *lastBlockIndexes(int sideSize, int blockCount, DivideType type) {

	char *fullPath = currentPath(sideSize, blockCount, type);
	FILE *file = fopen(fullPath, "r");
    
    if (file == NULL) {
        return NULL;
    }
    
	char *str = NULL;
    char *prevStr = NULL;
    char str1[100];
    char str2[100];
    char tmpStr[100];
    int count = 0;
    
    while (!feof(file)) {

        char *tmpC = fgets(tmpStr, 100, file);
        
        if (tmpC == NULL) {
            continue;
        }
        
		count++;

        if (count % 2 == 0) {
            strcpy(str2, tmpStr);
        } else {
            strcpy(str1, tmpStr);
        }
    }
    
    if (count == 0) {
        return NULL;
    }
    
    if (count % 2 == 0) {
        prevStr = str1;
        str = str2;
    } else {
        prevStr = str2;
        str = str1;
    }
    
    if (!checkLastStr(str)) {
        
        if (checkLastStr(prevStr)) {
            str = prevStr;
            count -= 2;
            
        } else {
            
            perror("undefined error");
            return NULL;
        }
        
    } else {
        
    	count--;
    }

    int *indexes = malloc(sizeof(indexes) * blockCount);
    int tmpNumber = 0;
    int numberCount = 0;
    int i = 0;
    char c;

    do {
        c = str[i++];
        
        if (c == SEPARATOR) {

            indexes[numberCount++] = tmpNumber;
            tmpNumber = 0;
            continue;
        }
        tmpNumber *= 10;
        tmpNumber += (int)c - 48;
        
    } while (checkTerminator(c) == false && (numberCount < blockCount));
    
	fclose(file);

	char dest[100];
	strcpy(dest, fullPath);
    strcat(dest, "1");
	copyFile(fullPath, dest, count);

    return indexes;
}

bool checkLastStr(char *s) {
    if (s == NULL) {
        return false;
    }
    
    size_t strLenght = strlen(s);
    if (strLenght < 3) {
        return false;
    }
    
    int i = (int)strLenght - 1;
    char lastChar = *(s + i);
    while (checkTerminator(lastChar)) {
        i--;
        lastChar = *(s + i);
    }
    
    return lastChar == END_OF_RECORD;
}

bool checkTerminator(char c) {
    return c == '\n' || c == '\0';
}

void copyFile(char *sourceName, char *destName, int strCount) {
	FILE *fSource = fopen(sourceName, "r");
	FILE *fDest = fopen(destName, "w");

    for (int i = 0; i < strCount; i++) {
	    char str[100];
	    fgets(str, 100, fSource);
	    fprintf(fDest, "%s", str);
    }

    fclose(fSource);
    fclose(fDest);

    remove(sourceName);
    rename(destName, sourceName);
}

void writeCombination(int sideSize, int **indexes,  int countOfIndexes, int countInRecord, DivideType type) {
    char *fullPath = currentPath(sideSize, countInRecord - 1, type);
    FILE *file = fopen(fullPath, "a");

    for (int i = 0; i < countOfIndexes; i++) {
        fprintf(file, "\n");
        int *indexesRecord = *(indexes + i);
        for (int j = 0; j < countInRecord; j++) {
            fprintf(file, "%d", *(indexesRecord + j));
            fprintf(file, "%c", SEPARATOR);
        }
        fprintf(file, "%c", END_OF_RECORD);
    }
    
    fclose(file);
}

char *currentPath(int sideSize, int blockCount, DivideType type) {
	static const int strLenght = 100;

    char fileName[10] = "xxxxx.txt";
    fileName[0] = sideSize + '0';
    fileName[1] = '_';
    fileName[2] = blockCount + '0';
    fileName[3] = '_';
    fileName[4] = type + '0';

    char *fullPath = malloc(sizeof(fullPath) * strLenght);
    fullPath = strcat(strcpy(fullPath, BASE_PATH), fileName);

    return fullPath;
}
